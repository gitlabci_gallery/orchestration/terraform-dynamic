# 2023-02-13

- !2 Update documentation after terraform MR 1 changes:
  terraform project now use destruct provisioner to unregister gitlab runner
  too.
  https://gitlab.inria.fr/gitlabci_gallery/orchestration/terraform/-/merge_requests/1
